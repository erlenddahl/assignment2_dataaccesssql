﻿using Assignment2_CSharpSql.DataAccess;
using Assignment2_CSharpSql.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace Assignment2_CSharpSql
{
    public class Program
    {
        static void Main(string[] args)
        {
            CheckAllTasks();
        }

        /**<summary>
         * Print list of customers
         * </summary>*/
        static void PrintCustomers(List<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        /**<summary>
         * Print customer
         * </summary>*/
        static void PrintCustomer(Customer c)
        {
            Console.WriteLine($"{c.CustomerId}, " +
                $"{c.FirstName}, " +
                $"{c.LastName}, " +
                $"{c.Country}, " +
                $"{c.PostalCode}, " +
                $"{c.Phone}, " +
                $"{c.Email}");
        }

        /**<summary>
         * Run all methods in the repository classes
         * </summary>*/
        public static void CheckAllTasks()
        {
            ICustomerRepository cRep = new CustomerRepository();
            ICustomerCountryRepository ccRep = new CustomerCountryRepository();
            ICustomerSpenderRepository csRep = new CustomerSpenderRepository();
            ICustomerGenreRepository cgRep = new CustomerGenreRepository();

            Console.WriteLine("Get all customers:");
            PrintCustomers(cRep.GetAllCustomers());

            Console.WriteLine("\nGet customer by id:");
            PrintCustomer(cRep.GetCustomer(1));

            Console.WriteLine("\nGet customer by name:");
            PrintCustomer(cRep.GetCustomer("Steve Murray"));

            Console.WriteLine("\nGet page of customers:");
            PrintCustomers(cRep.GetSomeCustomers(30, 20));

            Console.WriteLine("\nGet added customer:");
            cRep.AddCustomer(new Customer()
            {
                FirstName = "Add",
                LastName = "Customer",
                Country = "Norway",
                PostalCode = "2345",
                Phone = "1235678",
                Email = "email@email.com"
            });
            PrintCustomer(cRep.GetCustomer("Add Customer"));

            Console.WriteLine("\nGet updated customer:");
            cRep.UpdateCustomerPhone(1, "11111111");
            PrintCustomer(cRep.GetCustomer(1));

            Console.WriteLine("\nGet number of customers in each country:");
            List<CustomerCountry> customerCountries = ccRep.GetCustomerCountriesCount();
            foreach (CustomerCountry custcon in customerCountries)
            {
                Console.WriteLine($"{custcon.Name} : {custcon.CustomersFromCountry}");
            }

            Console.WriteLine("\nGet highest spending customers:");
            List<CustomerSpender> customerSpenders = csRep.GetTopCustomerSpenders();
            foreach (CustomerSpender customerSpender in customerSpenders)
            {
                Console.WriteLine($"{customerSpender.CustomerId} : {customerSpender.TotalSum}");
            }

            Console.WriteLine("\nGet customers favorite genre:");
            Console.WriteLine("One top genre:");
            CustomerGenre customerGenre = cgRep.GetCustomerGenre(13);
            Console.Write(customerGenre.CustomerId + " : ");
            foreach (string genre in customerGenre.TopGenres)
            {
                Console.WriteLine(genre);
            }
            Console.WriteLine("Tied top genres:");
            customerGenre = cgRep.GetCustomerGenre(12);
            Console.Write(customerGenre.CustomerId + " : ");
            foreach (string genre in customerGenre.TopGenres)
            {
                Console.WriteLine(genre);
            }
        }
    }
}
