﻿namespace Assignment2_CSharpSql.Models
{
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public decimal? TotalSum { get; set; }
    }
}
