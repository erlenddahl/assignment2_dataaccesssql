﻿using System.Collections.Generic;

namespace Assignment2_CSharpSql.Models
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public List<string> TopGenres { get; set; }
    }
}
