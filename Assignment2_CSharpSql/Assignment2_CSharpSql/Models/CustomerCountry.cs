﻿namespace Assignment2_CSharpSql.Models
{
    public class CustomerCountry
    {
        public string Name { get; set; }
        public int? CustomersFromCountry { get; set; }
    }
}
