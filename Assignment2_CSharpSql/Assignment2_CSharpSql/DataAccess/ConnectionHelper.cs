﻿using Microsoft.Data.SqlClient;

namespace Assignment2_CSharpSql.DataAccess
{
    public class ConnectionHelper
    {
        /**<summary>
         * Get connection string to your sql database
         * </summary>
         * <returns>Connectionstring</returns>*/
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "N-NO-01-03-3400\\SQLEXPRESS"; //Your server address
            connectStringBuilder.InitialCatalog = "Chinook";
            connectStringBuilder.IntegratedSecurity = true;
            return connectStringBuilder.ConnectionString;
        }
    }
}
