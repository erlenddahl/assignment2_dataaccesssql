﻿using Assignment2_CSharpSql.Models;
using System.Collections.Generic;

namespace Assignment2_CSharpSql.DataAccess
{
    public interface ICustomerCountryRepository
    {
        /**<summary>
         * Get all countries customers are from and how many are from each
         * </summary>
         * <returns> List of CustomerCountry </returns>
         * <exception cref="SqlException"></exception>*/
        public List<CustomerCountry> GetCustomerCountriesCount();
    }
}
