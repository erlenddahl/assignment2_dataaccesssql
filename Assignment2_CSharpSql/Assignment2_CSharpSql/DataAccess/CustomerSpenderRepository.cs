﻿using Assignment2_CSharpSql.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace Assignment2_CSharpSql.DataAccess
{
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetTopCustomerSpenders()
        {
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();
            try
            {
                string sql = "SELECT CustomerId, SUM(Total) as TotalSum " +
                    "FROM Invoice " +
                    "GROUP BY CustomerId " +
                    "ORDER BY TotalSum DESC";
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender customerSpender = new CustomerSpender()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    TotalSum = reader.IsDBNull(1) ? null : reader.GetDecimal(1)
                                };
                                customerSpenders.Add(customerSpender);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message); ;
            }
            return customerSpenders;
        }
    }
}
