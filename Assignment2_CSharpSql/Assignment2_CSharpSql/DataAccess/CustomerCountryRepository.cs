﻿using Assignment2_CSharpSql.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace Assignment2_CSharpSql.DataAccess
{
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        public List<CustomerCountry> GetCustomerCountriesCount()
        {
            List<CustomerCountry> customerCountries = new List<CustomerCountry>();
            try
            {
                string sql = "SELECT Country, COUNT(Country) as Number " +
                    "FROM Customer " +
                    "GROUP BY Country " +
                    "ORDER BY Number DESC";
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new CustomerCountry()
                                {
                                    Name = reader.GetString(0),
                                    CustomersFromCountry = reader.IsDBNull(1) ? null : reader.GetInt32(1)
                                };
                                customerCountries.Add(customerCountry);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message); ;
            }
            return customerCountries;
        }
    }
}
