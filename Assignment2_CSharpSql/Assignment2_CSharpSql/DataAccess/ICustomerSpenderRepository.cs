﻿using Assignment2_CSharpSql.Models;
using System.Collections.Generic;

namespace Assignment2_CSharpSql.DataAccess
{
    public interface ICustomerSpenderRepository
    {
        /**<summary>
         * Get list of CustomerSpender sorted by top spender  
         * </summary>
         * <returns> List of CustomerSpender </returns>
         * <exception cref="SqlException"></exception>*/
        public List<CustomerSpender> GetTopCustomerSpenders();
    }
}
