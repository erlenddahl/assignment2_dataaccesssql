﻿using Assignment2_CSharpSql.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace Assignment2_CSharpSql.DataAccess
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        public CustomerGenre GetCustomerGenre(int id)
        {
            CustomerGenre customerGenre = new CustomerGenre()
            {
                CustomerId = id,
                TopGenres = new List<string>()
            };
            try
            {
                string sql = "SELECT TOP 1 WITH TIES Genre.Name " +
                    "FROM Invoice " +
                    "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                    "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                    "INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                    $"WHERE Invoice.CustomerId = @id " +
                    "GROUP BY Genre.Name " +
                    "ORDER BY COUNT(Genre.Name) DESC";
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customerGenre.TopGenres.Add(reader.IsDBNull(0) ? "" : reader.GetString(0));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message); ;
            }
            return customerGenre;
        }
    }
}
