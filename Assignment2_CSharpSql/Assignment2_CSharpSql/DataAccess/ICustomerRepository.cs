﻿using Assignment2_CSharpSql.Models;
using System.Collections.Generic;

namespace Assignment2_CSharpSql.DataAccess
{
    public interface ICustomerRepository
    {
        /**<summary>
         * Get list of all customers
         * </summary>
         * <returns> List of Customer </returns>*/
        public List<Customer> GetAllCustomers();

        /**<summary>
         * Get list of @limit customers starting with customerId @offset+1
         * </summary>
         * <returns> List of Customer </returns>*/
        public List<Customer> GetSomeCustomers(int row, int start);

        /**<summary>
         * Get a customer by id
         * </summary>
         * <returns> Customer </returns>*/
        public Customer GetCustomer(int id);

        /**<summary>
         * Get a customer by name
         * </summary>
         * <returns> Customer </returns>*/
        public Customer GetCustomer(string name);

        /**<summary>
         * Add a customer to the customer table
         * </summary>
         * <returns> True if customer got added </returns>
         * <exception cref="SqlException"></exception>*/
        public bool AddCustomer(Customer customer);

        /**<summary>
         * Update a customers phone number
         * </summary>
         * <returns> True if it updated </returns>
         * <exception cref="SqlException"></exception>*/
        public bool UpdateCustomerPhone(int id, string phone);
    }
}
