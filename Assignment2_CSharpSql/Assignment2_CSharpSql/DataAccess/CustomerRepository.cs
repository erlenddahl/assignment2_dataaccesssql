﻿using Assignment2_CSharpSql.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace Assignment2_CSharpSql.DataAccess
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer";
            List<Customer> customerList = new List<Customer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.IsDBNull(3) ? "" : reader.GetString(3),
                                    PostalCode = reader.IsDBNull(4) ? "" : reader.GetString(4),
                                    Phone = reader.IsDBNull(5) ? "" : reader.GetString(5),
                                    Email = reader.IsDBNull(6) ? "" : reader.GetString(6)
                                };
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message); ;
            }
            return customerList;
        }

        public List<Customer> GetSomeCustomers(int limit, int offset)
        {
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "ORDER BY CustomerId " +
                "OFFSET @Offset ROWS " +
                "FETCH NEXT @Limit ROWS ONLY";
            List<Customer> customerList = new List<Customer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Limit", limit);
                        command.Parameters.AddWithValue("@Offset", offset);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.IsDBNull(3) ? "" : reader.GetString(3),
                                    PostalCode = reader.IsDBNull(4) ? "" : reader.GetString(4),
                                    Phone = reader.IsDBNull(5) ? "" : reader.GetString(5),
                                    Email = reader.IsDBNull(6) ? "" : reader.GetString(6)
                                };
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message); ;
            }
            return customerList;
        }

        public Customer GetCustomer(int id)
        {
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "WHERE CustomerId = @id";
            Customer customer = new Customer();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer = new Customer()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.IsDBNull(3) ? "" : reader.GetString(3),
                                    PostalCode = reader.IsDBNull(4) ? "" : reader.GetString(4),
                                    Phone = reader.IsDBNull(5) ? "" : reader.GetString(5),
                                    Email = reader.IsDBNull(6) ? "" : reader.GetString(6)
                                };
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message); ;
            }
            return customer;
        }

        public Customer GetCustomer(string name)
        {
            string firstName = name.Split()[0];
            string lastName = name.Split()[1];
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                $"WHERE FirstName LIKE @FirstName AND LastName LIKE @LastName";
            Customer customer = new Customer();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", "%"+firstName+"%");
                        command.Parameters.AddWithValue("@LastName", "%"+lastName+"%");
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer = new Customer()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.IsDBNull(3) ? "" : reader.GetString(3),
                                    PostalCode = reader.IsDBNull(4) ? "" : reader.GetString(4),
                                    Phone = reader.IsDBNull(5) ? "" : reader.GetString(5),
                                    Email = reader.IsDBNull(6) ? "" : reader.GetString(6)
                                };
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message); ;
            }
            return customer;
        }

        public bool AddCustomer(Customer c)
        {
            bool added = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", c.FirstName);
                        command.Parameters.AddWithValue("@LastName", c.LastName);
                        command.Parameters.AddWithValue("@Country", c.Country);
                        command.Parameters.AddWithValue("@PostalCode", c.PostalCode);
                        command.Parameters.AddWithValue("@Phone", c.Phone);
                        command.Parameters.AddWithValue("@Email", c.Email);

                        added = command.ExecuteNonQuery() == 0 ? false : true;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message); ;
            }
            return added;
        }

        public bool UpdateCustomerPhone(int id, string phone)
        {
            bool updated = false;
            string sql = "UPDATE Customer " +
                "SET Phone = @Phone " +
                "WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Phone", phone);
                        command.Parameters.AddWithValue("@CustomerId", id);

                        updated = command.ExecuteNonQuery() == 0 ? false : true;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return updated;
        }
    }
}
