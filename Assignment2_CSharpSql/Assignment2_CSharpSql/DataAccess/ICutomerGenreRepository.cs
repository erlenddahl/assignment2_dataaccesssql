﻿using Assignment2_CSharpSql.Models;

namespace Assignment2_CSharpSql.DataAccess
{
    public interface ICustomerGenreRepository
    {
        /**<summary>
         * Get a customers top genre/genres as CustomerGenre
         * </summary>
         * <returns> CustomerGenre </returns>
         * <exception cref="SqlException"></exception>*/
        public CustomerGenre GetCustomerGenre(int id);
    }
}
