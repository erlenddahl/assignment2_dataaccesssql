USE SuperheroesDb

CREATE TABLE Superhero (
    Id      INT           NOT NULL    IDENTITY    PRIMARY KEY,
    Alias    VARCHAR(100),
    Origin   VARCHAR(100),
    Name  VARCHAR(100),
);

CREATE TABLE Assistant (
    Id      INT           NOT NULL    IDENTITY    PRIMARY KEY,
    Name VARCHAR(100),
);

CREATE TABLE Power (
    Id          INT           NOT NULL    IDENTITY    PRIMARY KEY,
    Description        VARCHAR(100),
    Name VARCHAR(100),
);