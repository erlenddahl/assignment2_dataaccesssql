INSERT INTO Superhero (Alias, Origin, Name)
VALUES 
    ('Tony Stark', 'Earth', 'Iron Man'),
    ('Clark Kent', 'Krypton', 'Superman'),
    ('Arthur Curry', 'Atlantis', 'Aquaman');