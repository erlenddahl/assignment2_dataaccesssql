CREATE TABLE SuperheroPowers (
    SuperheroId INT FOREIGN KEY REFERENCES Superhero(Id),
    PowerId INT FOREIGN KEY REFERENCES Power(Id),
    PRIMARY KEY (SuperheroId, PowerId)
);