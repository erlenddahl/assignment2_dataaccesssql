INSERT INTO Power (Name, Description)
VALUES 
    ('Invisibility', 'Invisibility'),
    ('Thunder', 'Thunder'),
    ('X-ray', 'X-ray vision'),
    ('Mind reading', 'Mind reading');

INSERT INTO SuperheroPowers (SuperheroId, PowerId)
VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (1, 4),
    (2, 1),
    (2, 2),
    (2, 3),
    (2, 4),
    (3, 1),
    (3, 2),
    (3, 3),
    (3, 4);