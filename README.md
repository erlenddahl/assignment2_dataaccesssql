# DataAccessSql

This project is in two parts. Part one is to create a database with tables and relationships using sql query scripts.
Part two of the project is to read and write data to a sql database using SQL Client with C#.

### Part 1
To run part one you have to run all the sql query scripts in the SqlQueries folder starting with the lowest number.
This creates the database called SuperheroesDb.

### Part 2
To run part two of the project you can run the Main method in program to check that all the methods works as they should. 
For this to work you first have to run the Chinook script to create the database and then set your server address in ConnectionHelper where it is commented.

# Contributors
Erlend Halsne Dahl (@Erlend-Halsne-Dahl)
Meg Stefouli (@meglicious)